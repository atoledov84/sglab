﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Infrastructure.Pagination
{
    public class PagedResult<T> : PagedResultBase where T : class
    {
        public IList<T> Data { get; set; }

        public PagedResult()
        {
            Data = new List<T>();
        }
    }
}
