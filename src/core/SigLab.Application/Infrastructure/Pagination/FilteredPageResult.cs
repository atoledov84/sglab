﻿using AutoMapper;
using SigLab.Aplicacion.Interface.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Infrastructure.Pagination
{
    public class FilteredPagedResult<T> where T : class 
    {
        public FilteredPagedResult():base()
        {
        }

        public string Field { get; set; }
        public string Value { get; set; }

        public PagedResult<T> Results { get; set; }
    }
}

