﻿using SigLab.Aplicacion.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SigLab.Aplicacion.Interface
{
    public interface INotificacionService
    {
        Task SendAsync(Mensaje message);
    }
}
