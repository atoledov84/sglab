﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SigLab.Dominio.Catalogo;

namespace SigLab.Application.Interface
{
    public interface IAppDbContext
    {
        DbSet<Convenio> Convenios { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
