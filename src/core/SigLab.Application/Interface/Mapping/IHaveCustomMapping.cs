﻿using AutoMapper;

namespace SigLab.Aplicacion.Interface.Mapping
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}
