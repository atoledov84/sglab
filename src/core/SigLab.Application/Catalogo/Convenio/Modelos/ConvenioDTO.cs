﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SigLab.Aplicacion.Interface.Mapping;
using SigLab.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Catalogo.Convenio.Modelos
{
    public class ConvenioDTO : IMapFrom<Dominio.Catalogo.Convenio>
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumEstado Estado { get; set; }
    }
}
