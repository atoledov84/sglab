﻿using SigLab.Application.Infrastructure.Pagination;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Catalogo.Convenio.Modelos
{
    public class ListaConveniosDTO : PagedResult<ConvenioDTO>
    {
        public ListaConveniosDTO() : base()
        {

        }
    }
}
