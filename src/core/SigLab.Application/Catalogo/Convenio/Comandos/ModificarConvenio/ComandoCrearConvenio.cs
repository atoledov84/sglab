﻿using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SigLab.Application.Interface;
using SigLab.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Comandos.ModificarConvenio
{
    public class ComandoModificarConvenio : IRequest<int>
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumEstado Estado { get; set; }

        class ComandoModificarConvenioHandler : IRequestHandler<ComandoModificarConvenio, int>
        {

            private readonly IAppDbContext context;
            private readonly IMediator mediator;


            public ComandoModificarConvenioHandler(
                IAppDbContext context,
                IMediator mediator)
            {
                this.context = context;
                this.mediator = mediator;
            }

            public async Task<int> Handle(ComandoModificarConvenio request, CancellationToken cancellationToken)
            {

                Dominio.Catalogo.Convenio result = context.Convenios.Find(request.Id);

                if (result == null)
                {
                    throw new Exceptions.NotFoundException(nameof(Dominio.Catalogo.Convenio), request.Id);
                }

                result.Descripcion = request.Descripcion;
                result.Estado = request.Estado;
                result.UpdFecha = DateTime.UtcNow;
                result.UpdUserId = 0;

                return await context.SaveChangesAsync(cancellationToken);

            }

        }

    }
}
