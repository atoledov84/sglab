﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Catalogo.Convenio.Comandos.ModificarConvenio
{
    public class ValidaModicacionConvenio : AbstractValidator<ComandoModificarConvenio>
    {

        public ValidaModicacionConvenio()
        {
            RuleFor(p => p.Descripcion)
                .NotNull()
                .MaximumLength(150);
            RuleFor(p => p.Estado)
                .NotNull().IsInEnum();
        }

    }
}
