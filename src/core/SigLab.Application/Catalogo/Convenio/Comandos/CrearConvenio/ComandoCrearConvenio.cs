﻿using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SigLab.Application.Interface;
using SigLab.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Comandos.CrearConvenio
{
    public class ComandoCrearConvenio : IRequest<Unit>
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumEstado Estado { get; set; }

        class ComandoCrearConvenioHandler : IRequestHandler<ComandoCrearConvenio, Unit>
        {

            private readonly IAppDbContext context;
            private readonly IMediator mediator;


            public ComandoCrearConvenioHandler(
                IAppDbContext context,
                IMediator mediator)
            {
                this.context = context;
                this.mediator = mediator;
            }

            public async Task<Unit> Handle(ComandoCrearConvenio request, CancellationToken cancellationToken)
            {
                Dominio.Catalogo.Convenio data = new Dominio.Catalogo.Convenio
                {
                    Descripcion = request.Descripcion,
                    InsFecha = DateTime.UtcNow,
                    InsUserId = 0,
                    Estado = request.Estado
                };

                context.Convenios.Add(data);

                await context.SaveChangesAsync(cancellationToken);

                await mediator.Publish(new ConvenioCreado { Id = data.Id });

                return Unit.Value;

            }

        }

    }
}
