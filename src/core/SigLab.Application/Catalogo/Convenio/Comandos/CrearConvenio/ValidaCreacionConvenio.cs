﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Catalogo.Convenio.Comandos.CrearConvenio
{
    public class ValidaCreacionConvenio : AbstractValidator<ComandoCrearConvenio>
    {

        public ValidaCreacionConvenio()
        {
            RuleFor(p => p.Descripcion)
                .NotNull()
                .MaximumLength(150);
            RuleFor(p => p.Estado)
                .NotNull().IsInEnum();
        }

    }
}
