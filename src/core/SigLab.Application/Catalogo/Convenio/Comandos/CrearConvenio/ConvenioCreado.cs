﻿using MediatR;
using SigLab.Aplicacion.Interface;
using SigLab.Aplicacion.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Comandos.CrearConvenio
{
    public class ConvenioCreado : INotification
    {
        public int Id { get; set; }

        public class ConvenioCreadoHandler : INotificationHandler<ConvenioCreado>
        {
            private readonly INotificacionService _notification;

            public ConvenioCreadoHandler(INotificacionService notification)
            {
                _notification = notification;
            }

            public async Task Handle(ConvenioCreado notification, CancellationToken cancellationToken)
            {
                
                await _notification.SendAsync(new Mensaje {
                    De = "Convenios",
                    Para = "Cobranzas",
                    Asunto = "Creacion de convenio",
                    Contenido = "Id: " + notification.Id
                });
            }

        }
    }
}
