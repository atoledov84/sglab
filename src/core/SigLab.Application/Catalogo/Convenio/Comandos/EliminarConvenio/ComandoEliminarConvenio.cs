﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SigLab.Application.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Comandos.EliminarConvenio
{
    public class ComandoEliminarConvenio : IRequest<int>
    {

        public int Id { get; set; }

        class ComandoEliminarConvenioHandler : IRequestHandler<ComandoEliminarConvenio, int>
        {
            private readonly IAppDbContext context;
            private readonly IMapper mapper;

            public ComandoEliminarConvenioHandler(IAppDbContext context, IMapper mapper)
            {
                this.context = context;
                this.mapper = mapper;
            }

            public async Task<int> Handle(ComandoEliminarConvenio request, CancellationToken cancellationToken)
            {
                var result = await context.Convenios
                    .Where(o => o.Id == request.Id)
                    .SingleOrDefaultAsync();

                if (result == null)
                {
                    throw new Exceptions.NotFoundException(nameof(Dominio.Catalogo.Convenio), request.Id);
                }

                context.Convenios.Remove(result);

                return await context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}
