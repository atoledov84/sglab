﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Application.Catalogo.Convenio.Queries.ConsultarConvenios
{
    public class ValidacionConsultarConvenios : AbstractValidator<ConsultarConvenios>
    {

        public ValidacionConsultarConvenios()
        {
            RuleFor(p => p.Page).GreaterThan(0);
            RuleFor(p => p.PageSize).GreaterThan(0);
            RuleFor(p => p.Value).NotEmpty()
               .When(p => !string.IsNullOrEmpty(p.Field));

        }
    }
}
