﻿using AutoMapper;
using MediatR;
using SigLab.Application.Catalogo.Convenio.Modelos;
using SigLab.Application.Infrastructure.Pagination;
using SigLab.Application.Interface;
using SigLab.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Queries.ConsultarConvenios
{
    public class ConsultarConvenios : IRequest<ListaConveniosDTO>
    {

        public ConsultarConvenios()
        {
            PageSize = 10;
        }

        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }

        class ConsultarConveniosHandler : IRequestHandler<ConsultarConvenios, ListaConveniosDTO>
        {
            private readonly IAppDbContext context;
            private readonly IMapper mapper;
            private readonly IDateTime dateTime;

            public ConsultarConveniosHandler(IAppDbContext context, IMapper mapper, IDateTime dateTime)
            {
                this.context = context;
                this.mapper = mapper;
                this.dateTime = dateTime;
            }

            public async Task<ListaConveniosDTO> Handle(ConsultarConvenios request, CancellationToken cancellationToken)
            {
                var query = context.Convenios.AsQueryable();

                switch (request.Field)
                {
                    case "Estado":
                        Dominio.Enums.EnumEstado estado;
                        if (Enum.TryParse(request.Value, out estado))
                        {
                            query = query.Where(o => o.Estado == estado);
                        }
                        break;
                    case "Id":
                        query = query.Where(o => o.Id == Convert.ToInt32(request.Value));
                        break;
                    //case "FechaEntrega":
                    //    query = query.Where(o => o.FechaEntrega.HasValue &&
                    //        o.FechaEntrega.Value.Date == dateTime.From(request.Value));
                    //    break;
                    case "Descripcion":
                        query = query.Where(o => o.Descripcion.Contains(request.Value));
                        break;
                }
                               
                return mapper.Map<ListaConveniosDTO>(await query.GetPagedAsync(request.Page.HasValue ?
                        request.Page.Value : 1,
                        request.PageSize, cancellationToken));
            }
        }

    }
}
