﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SigLab.Application.Catalogo.Convenio.Modelos;
using SigLab.Application.Interface;
using SigLab.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Application.Catalogo.Convenio.Queries.DetalleConvenio
{
    public class DetalleConvenio : IRequest<ConvenioDTO>
    {

        public int Id { get; set; }

        class ConsultarConveniosHandler : IRequestHandler<DetalleConvenio, ConvenioDTO>
        {
            private readonly IAppDbContext context;
            private readonly IMapper mapper;
            
            public ConsultarConveniosHandler(IAppDbContext context, IMapper mapper)
            {
                this.context = context;
                this.mapper = mapper;
            }

            public async Task<ConvenioDTO> Handle(DetalleConvenio request, CancellationToken cancellationToken)
            {
                var result = await context.Convenios
                    .Where(o => o.Id == request.Id)
                    .SingleOrDefaultAsync();

                if (result == null)
                {
                    throw new Exceptions.NotFoundException(nameof(Dominio.Catalogo.Convenio), request.Id);
                }

                return mapper.Map<ConvenioDTO>(result);

            }
        }
    }
}
