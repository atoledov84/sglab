﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Aplicacion.Notifications.Model
{
    public class NotificacionMensaje : INotification
    {
        public NotificacionMensaje(string mensaje)
        {
            Mensaje = mensaje;
        }

        public string Mensaje { get; set; }
    }
}
