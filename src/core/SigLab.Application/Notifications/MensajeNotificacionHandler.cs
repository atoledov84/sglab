﻿using SigLab.Aplicacion.Notifications.Model;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Aplicacion.Notifications
{
    public class MensajeNotificacionHandler : INotificationHandler<NotificacionMensaje>
    {
        private readonly ILogger<MensajeNotificacionHandler> _logger;

        public MensajeNotificacionHandler(ILogger<MensajeNotificacionHandler> logger)
        {
            _logger = logger;
        }
       
        public Task Handle(NotificacionMensaje notification, CancellationToken cancellationToken)
        {
            _logger.LogWarning($"Handled: {notification.Mensaje}");
            return Task.CompletedTask;
        }
    }
}
