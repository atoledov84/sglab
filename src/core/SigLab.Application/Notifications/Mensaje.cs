﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Aplicacion.Notifications
{
    public class Mensaje
    {
        public string De { get; set; }
        public string Para { get; set; }
        public string Asunto { get; set; }
        public string Contenido { get; set; }
    }
}
