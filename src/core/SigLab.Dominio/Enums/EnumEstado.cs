﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Dominio.Enums
{
    public enum EnumEstado
    {
        ACTIVO,
        INACTIVO,
        PENDIENTE_ELIMINACION
    }
}
