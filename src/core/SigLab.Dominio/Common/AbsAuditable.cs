﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Dominio.Common
{
    public abstract class AbsAuditable
    {
        public int InsUserId { get; set; }
        public DateTime InsFecha { get; set; }

        public int? UpdUserId { get; set; }
        public DateTime? UpdFecha { get; set; }
    }
}
