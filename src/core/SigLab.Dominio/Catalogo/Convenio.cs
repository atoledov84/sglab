﻿using SigLab.Dominio.Common;
using SigLab.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Dominio.Catalogo
{
    public class Convenio : AbsAuditable
    {

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public EnumEstado Estado { get; set; }

    }
}
