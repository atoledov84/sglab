﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Dominio.Excepctions
{
    public class ModificacionNoPermitidaException : Exception
    {
        public ModificacionNoPermitidaException(string Mensaje) : base(Mensaje)
        {

        }


    }
}
