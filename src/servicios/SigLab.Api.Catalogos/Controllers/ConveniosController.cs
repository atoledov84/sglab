﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SigLab.Application.Catalogo.Convenio.Comandos.CrearConvenio;
using SigLab.Application.Catalogo.Convenio.Comandos.EliminarConvenio;
using SigLab.Application.Catalogo.Convenio.Comandos.ModificarConvenio;
using SigLab.Application.Catalogo.Convenio.Modelos;
using SigLab.Application.Catalogo.Convenio.Queries.ConsultarConvenios;
using SigLab.Application.Catalogo.Convenio.Queries.DetalleConvenio;
using SigLab.Application.Infrastructure.Pagination;

namespace SigLab.Api.Catalogos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConveniosController : BaseController
    {
        // GET: api/Convenios
        [HttpGet]
        [ProducesResponseType(typeof(PagedResult<ConvenioDTO>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagedResult<ConvenioDTO>>> Get([FromQuery] ConsultarConvenios request)
        {
            return Ok(await Mediator.Send(request));
        }

        // GET: api/Convenios/5
        [HttpGet("{id}", Name = "GetConvenio")]
        [ProducesResponseType(typeof(ConvenioDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ConvenioDTO>> Get(int id)
        {
            return Ok(await Mediator.Send(new DetalleConvenio { Id = id }));
        }

        // POST: api/Convenios
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] ComandoCrearConvenio command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        // PUT: api/Convenios/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Put(int id, [FromBody] ComandoModificarConvenio command)
        {
            command.Id = id;
            var respuesta = await Mediator.Send(command);
            return Ok(respuesta);
        }

        // DELETE: api/Convenios/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var respuesta = await Mediator.Send(new ComandoEliminarConvenio { Id = id });
            return NoContent();
        }
    }
}
