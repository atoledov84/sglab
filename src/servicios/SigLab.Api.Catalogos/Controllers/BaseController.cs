﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SigLab.Common;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace SigLab.Api.Catalogos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private IMediator _mediator;

        protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());

        private IEnumHelper _enumHelper;

        protected IEnumHelper EnumHelper => _enumHelper ?? (_enumHelper = HttpContext.RequestServices.GetService<IEnumHelper>());
    }
}