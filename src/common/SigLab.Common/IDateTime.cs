﻿using System;

namespace SigLab.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }

        DateTime From(string date);

        DateTime From(long date);
    }
}
