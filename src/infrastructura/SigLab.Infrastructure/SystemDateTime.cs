﻿using SigLab.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SigLab.Infrastructure
{
    public class SystemDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;

        public int CurrentYear => DateTime.Now.Year;

        public DateTime From(string date)
        {
            return DateTime.ParseExact(date,
                new string[] { "MM.dd.yyyy", "MM-dd-yyyy", "MM/dd/yyyy"
                , "yyyy/MM/ddTHH:mm:ss", "yyyy-MM-ddTHH:mm:ss", "yyyy/MM/dd HH:mm:ss" },
                CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        public DateTime From(long date)
        {
            return new DateTime(date);
        }
    }
}
