﻿using SigLab.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Infrastructure
{
    public class EnumHelper : IEnumHelper
    {
        public List<object> Catalog<T>(T enumarted) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            var enumVals = new List<object>();
            Type type = typeof(T);
            foreach (var item in Enum.GetValues(type))
            {
                enumVals.Add(new
                {
                    id = (int)item,
                    name = item.ToString()
                });
            }
            return enumVals;
        }

        public List<object> Catalog(Type type)
        {
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            var enumVals = new List<object>();
            
            foreach (var item in Enum.GetValues(type))
            {
                enumVals.Add(new
                {
                    id = (int)item,
                    name = item.ToString()
                });
            }
            return enumVals;
        }
    }
}
