﻿using SigLab.Aplicacion.Interface;
using SigLab.Aplicacion.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace SigLab.Infrastructure
{
    public class NotificacionService : INotificacionService
    {
        private readonly ILogger logger;

        public NotificacionService(ILogger logger)
        {
            this.logger = logger;
        }

        public Task SendAsync(Mensaje message)
        {
            logger.LogInformation("Se ebio enviar el siguiente mensaje: {1}", message.Contenido);

            return Task.CompletedTask;
        }
    }
}
