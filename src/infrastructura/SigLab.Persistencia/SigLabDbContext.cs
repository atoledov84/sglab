﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using SigLab.Application.Interface;
using SigLab.Dominio.Catalogo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SigLab.Persistencia
{
    public class SigLabDbContext : DbContext, IAppDbContext
    {
        public SigLabDbContext(DbContextOptions<SigLabDbContext> options) : base(options)
        {
        }

        public DbSet<Convenio> Convenios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SigLabDbContext).Assembly);
        }
    }
}
