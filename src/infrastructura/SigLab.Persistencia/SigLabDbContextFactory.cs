﻿using Microsoft.EntityFrameworkCore;
using SigLab.Persistencia.Infraestructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Persistencia
{
    public class SigLabDbContextFactory : DesignTimeSigLabDbContext<SigLabDbContext>
    {
        protected override SigLabDbContext CreateNewInstance(DbContextOptions<SigLabDbContext> options)
        {
            return new SigLabDbContext(options);
        }

    }
}
