﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SigLab.Dominio.Catalogo;
using System;
using System.Collections.Generic;
using System.Text;

namespace SigLab.Persistencia.Configurations
{
    class ConvenioEfConfiguration : IEntityTypeConfiguration<Convenio>
    {
        public void Configure(EntityTypeBuilder<Convenio> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("ConvenioCatalogo");
            builder.Property(e => e.Id).HasColumnName("ConvenioId").ValueGeneratedOnAdd();

            builder.Property(e => e.Descripcion).HasColumnName("Descripcion")
                .IsRequired()
                .HasMaxLength(250);

            
        }
    }
}
