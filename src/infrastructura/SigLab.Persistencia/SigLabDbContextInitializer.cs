﻿using SigLab.Dominio.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SigLab.Persistencia
{
    public class SigLabDbContextInitializer
    {
        private readonly Dictionary<int, Convenio> Convenios = new Dictionary<int, Convenio>();

        public static void Initialize(SigLabDbContext context)
        {
            var initializer = new SigLabDbContextInitializer();
            initializer.SeedTodo(context);
        }

        public void SeedTodo(SigLabDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Convenios.Any())
            {
                return; // Db has been seeded
            }

            SeedConvenios(context);
        }
        
        public void SeedConvenios(SigLabDbContext context)
        {

            Convenios.Add(1, new Convenio
            {
                Estado = Dominio.Enums.EnumEstado.ACTIVO,
                Descripcion = "Convenio de Prueba",
                InsFecha = DateTime.UtcNow,
                InsUserId = 0
            });

            foreach (var convenio in Convenios.Values)
            {
                context.Convenios.Add(convenio);
            }

            context.SaveChanges();

        }

        public static string MD5Hash(string input)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(input));
                return Encoding.ASCII.GetString(result);
            }
        }
    }
}
