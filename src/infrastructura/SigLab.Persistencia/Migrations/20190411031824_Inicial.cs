﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SigLab.Persistencia.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConvenioCatalogo",
                columns: table => new
                {
                    ConvenioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsUserId = table.Column<int>(nullable: false),
                    InsFecha = table.Column<DateTime>(nullable: false),
                    UpdUserId = table.Column<int>(nullable: true),
                    UpdFecha = table.Column<DateTime>(nullable: true),
                    Descripcion = table.Column<string>(maxLength: 250, nullable: false),
                    Estado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConvenioCatalogo", x => x.ConvenioId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConvenioCatalogo");
        }
    }
}
